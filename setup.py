from setuptools import find_packages, setup

setup(
    name='netbox-mailreporter',
    version='0.1.7',
    description='Netbox Plugin - Mail Reporter',
    long_description='Netbox Plugin - Mail Reporter',
    url='',
    download_url='',
    author='Roman Alexander Mariančík',
    author_email='492965@muni.cz',
    license='Apache 2.0',
    install_requires=[],
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Framework :: Django",
    ],
    python_requires=">=3.8",
    zip_safe=False,
)
