from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _
from netbox.models import NetBoxModel


class MailReporter(NetBoxModel):
    name = models.CharField(max_length=255)
    table = models.ForeignKey(to=ContentType, on_delete=models.PROTECT,
                              help_text='The type of NetBox object that is going to be watched for changes')
    fields = models.JSONField(
        help_text="The fields of the table that are being watched for changes and included in the email response")
    filters = models.JSONField(blank=True, null=True, default=dict,
                               help_text="A set of conditions that constrain "
                                         "the objects that are watched for changes."
                                         "Use JSON format to define filters, entries inside one JSON block "
                                         "are ANDed, separating multiple blocks with \'|\'"
                                         "is interpreted as OR.")
    email_address = models.CharField(max_length=1023)
    email_subject = models.CharField(max_length=255,
                                     help_text="You can use any valid Jinja2 syntax. "
                                               "The max length of this field is 255. "
                                               "This MailReporter object is stored in the \'reporter\' variable. "
                                               "Objects and changes that have been made since "
                                               "last report are stored in the \'objects\' variable.")
    email_content = models.TextField(blank=True, null=True,
                                     default="""{%- for obj, changes in select(objects) %}
    ---EMAIL---
{%- endfor %}""",
                                     help_text="You can use any valid Jinja2 syntax. "
                                               "This MailReporter object is stored in the \'reporter\' variable. "
                                               "Objects and changes that have been made since "
                                               "last report are stored in the \'objects\' variable. "
                                               "Function \'get_json(objects)\' converts objects to a JSON string.")
    email_attachment = models.TextField(blank=True, null=True,
                                        default="""{{ get_json(reporter, objects) }}""",
                                        help_text="You can use any valid Jinja2 syntax. "
                                                  "This MailReporter object is stored in the \'reporter\' variable. "
                                                  "Objects and changes that have been made since "
                                                  "last report are stored in the \'objects\' variable. "
                                                  "Function \'get_json(objects)\' converts objects to a JSON string.")
    email_attachment_filename = models.CharField(max_length=255, blank=True, null=True,
                                                 default="changes.json",
                                                 help_text="A default filename is \'changes.json\'")
    last_sent = models.DateTimeField(blank=True, null=True)
    last_changes = models.JSONField(blank=True, null=True)
    status = models.CharField(max_length=50, default="CREATED")
    message = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse("plugins:netbox_mailreporter:mailreporter", args=[self.pk])

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

# def save(self, *args, **kwargs):
#     from netbox_mailreporter.signals import delete_callback
#     if self.deletions:
#         pre_delete.connect(delete_callback, sender=apps.get_model(self.table.app_label, self.table.model))
#     super(MailReporter, self).save(*args, **kwargs)
#
# def delete(self, *args, **kwargs):
#     from netbox_mailreporter.signals import delete_callback
#     if self.deletions:
#         pre_delete.disconnect(delete_callback, sender=apps.get_model(self.table.app_label, self.table.model))
#     super(MailReporter, self).delete(*args, **kwargs)
