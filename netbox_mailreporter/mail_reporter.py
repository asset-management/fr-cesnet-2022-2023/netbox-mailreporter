import traceback

from django.forms.models import model_to_dict
import copy
from threading import Thread
from django.db import models
from extras.plugins import get_plugin_config
from netbox_mailreporter.status_codes import *
from django.apps import apps
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.utils import timezone
from jinja2 import Template
from extras.models import ObjectChange
from netbox_mailreporter.models import MailReporter
import json
from utilities.querysets import RestrictedQuerySet
from django.core.mail import EmailMessage


def check_fields(sender, fields):
    """
    Checks field for existence
    @param sender: model
    @param fields: fields that should be defined on the model
    @return: True/False
    """
    for field in fields:
        if not hasattr(sender, field):
            return False, field
    return True, ""


def filter_objects(m, filters):
    """
    Filter used for horizontal elimination of objects, e.g. only objects that satisfy the constraints are included
    in the monitoring for changes.
    @param m: model
    @param filters: a list of dictionaries
    @return: objects that got through the filter
    """
    q_filter = Q()
    if isinstance(filters, dict):
        q_filter = Q(**filters)
    else:
        for f in filters:
            filter_dict = f
            q_filter |= Q(**filter_dict)
    return m.objects.filter(q_filter)


def handle_create(reporter, obj):
    """
    Handles an object that was created and not changed
    @param reporter: reporter
    @param obj: object
    @return: True if changed, a summary of changes
    """
    summary = {}
    for field in reporter.fields:
        current_data = getattr(obj, field)
        if isinstance(current_data, models.Model):
            val = model_to_dict(current_data)
        else:
            val = current_data
        summary[field] = ("", val)
    return True, summary


def get_fk_from_log(current_object, pk):
    """
    @param current_object: a referenced object
    @param pk: primary key of an object referenced from logs
    @return: a dictionary with values corresponding to the attributes of the referenced object from the change log.
    """
    fk_model = type(current_object)
    fk_obj = fk_model.objects.filter(pk=pk)
    if len(fk_obj) == 0:
        return None
    else:
        return model_to_dict(fk_obj.first())


def get_val_from_cf(current_data, last_log):
    """
    Obtains values from custom_field_data
    @param current_data: current data
    @param last_log: the most recent log
    @return: the same two arguments, but only the changed keys of the nested dictionary custom_field_data
    """
    last_custom_fields = copy.deepcopy(last_log.postchange_data["custom_fields"])
    current_custom_fields = copy.deepcopy(current_data)
    common_custom_field_keys = last_custom_fields.keys() & current_data.keys()
    current_data = {}
    last_data = {}
    for k in common_custom_field_keys:
        before_val = last_custom_fields[k]
        after_val = current_custom_fields[k]
        if before_val != after_val:
            current_data[k] = after_val
            last_data[k] = before_val
    return last_data, current_data


def get_update_summary(reporter, obj, last_log):
    """
    creates the summary of changes
    @param reporter: reporter
    @param obj: object
    @param last_log: the most recent log
    @return: True if changed, a summary of changes
    """
    summary = {}
    changed = False
    for field in reporter.fields:
        current_data = getattr(obj, field)
        if isinstance(current_data, models.Model):
            pk = last_log.postchange_data[field]
            if pk is not None and isinstance(pk, int):
                last_data = get_fk_from_log(current_data, pk)
            else:
                last_data = ""
            current_data = model_to_dict(current_data)
        elif field == "custom_field_data":
            last_data, current_data = get_val_from_cf(current_data, last_log)
        else:
            last_data = last_log.postchange_data[field]
        if last_data != current_data:
            summary[field] = (last_data, current_data)
            changed = True
    return changed, summary


def handle_update(reporter, obj, log: RestrictedQuerySet):
    """
    Handles an object that was updated
    @param reporter: reporter
    @param obj: object
    @param log: change log
    @return: True if changed, a summary of changes
    """
    ordered_log = log.order_by('time')
    # if reporter.last_sent is None and len(ordered_log) != 0:
    #     return get_update_summary(reporter, obj, ordered_log[len(ordered_log) - 1])
    i = 0
    while i < len(ordered_log) \
            and reporter.last_sent is not None \
            and ordered_log[i].time < reporter.last_sent:
        i += 1
    if i == len(ordered_log):
        return False, {}
    i -= 1
    if i < 0 or i >= len(ordered_log):
        return False, {}
    return get_update_summary(reporter, obj, ordered_log[i])


def report_single(reporter: MailReporter):
    """
    Report changes by running a singe mail reporter
    @param reporter: reporter
    @return: a dictionary of changed objects. The keys are objects, the values are again dictionaries (changes).
    Each nested dictionary (change) uses the attribute names as keys and values are tuples - value of the
    attribute before and after.
    """
    if reporter.status == CREATED:
        raise AssertionError("Mail reporter is not initialized.")
    set_status(reporter, RUNNING, "")
    reporter.last_changes = json.loads("{}")
    reporter.save()
    if reporter.last_sent is None:
        reporter.last_sent = timezone.now()
        reporter.save()
    changed_objects = {}
    m = apps.get_model(reporter.table.app_label, reporter.table.model)
    for obj in filter_objects(m, reporter.filters):
        changelog = ObjectChange.objects.filter(changed_object_id=obj.pk,
                                                changed_object_type=ContentType.objects.filter(
                                                    model=reporter.table.model,
                                                    app_label=reporter.table.app_label)
                                                .first())
        recent = changelog.first()
        if recent is None:
            continue
        if (recent.action == 'create' or recent.action == "update") \
                and changelog.last() == recent \
                and recent.time > reporter.last_sent:
            # and (reporter.last_sent is None or recent.time > reporter.last_sent):
            ret, summary = handle_create(reporter, obj)
            changed_objects[obj] = summary
        elif recent.action == 'update':
            ret, summary = handle_update(reporter, obj, changelog)
            if ret:
                changed_objects[obj] = summary
    if len(changed_objects) != 0:
        send(reporter, changed_objects)
    set_status(reporter, SUCCESS, f"{len(changed_objects)} changes reported at {timezone.now()}")
    print('Done')
    return changed_objects


def render_template(template, data):
    return Template(template).render(data)


def send(reporter, changed_objects):
    """
    Sends an email
    @param reporter: reporter
    @param changed_objects: changed objects, see the above function for expalantion
    @return: None
    """
    email_subject = render_template(reporter.email_subject,
                                    {"reporter": reporter, "objects": changed_objects, "get_json": get_json,
                                     "select": select_nested_fields})
    email_body = render_template(reporter.email_content,
                                 {"reporter": reporter, "objects": changed_objects, "get_json": get_json,
                                  "select": select_nested_fields})
    email = EmailMessage(subject=email_subject,
                         body=email_body,
                         from_email=get_plugin_config('netbox_mailreporter', 'FROM_EMAIL'),
                         to=reporter.email_address.split(' '))
    if reporter.email_attachment is not None and reporter.email_attachment != "":
        email_attachment = render_template(reporter.email_attachment,
                                           {"reporter": reporter, "objects": changed_objects, "get_json": get_json,
                                            "select": select_nested_fields})
        email.attach(reporter.email_attachment_filename, email_attachment)
    email.send()
    reporter.last_sent = timezone.now()
    reporter.save()


# The following functions are used in the Jinja2 environment.
def process_nested_fields(changes, fields):
    """
    Returns only desired attribute of foreign key objects
    @param changes: changes
    @param fields: a dictionary model : a list of fields to be included
    @return: modified changes
    """
    ch = {}
    for k, v in changes.items():
        if k in fields.keys():
            before_v = {}
            after_v = {}
            for nested_k in fields[k]:
                pre_val = v[0]
                post_val = v[1]
                if isinstance(pre_val, dict) and isinstance(post_val, dict):
                    before_v[nested_k], after_v[nested_k] = pre_val[nested_k], post_val[nested_k]
                elif isinstance(post_val, dict):
                    after_v[nested_k] = post_val[nested_k]
                elif isinstance(pre_val, dict):
                    before_v[nested_k] = pre_val[nested_k]

            ch[k] = (before_v, after_v)
        else:
            ch[k] = v
    return ch


def select_nested_fields(objects: dict, *args, **fields):
    """
    Returns only selected attribute of foreign key objects
    @param objects: changed objects
    @param args: not used
    @param fields: a dictionary model : a list of fields to be included
    @return: modified dictionary of changed objects
    """
    selected = {}
    for obj, changes in objects.items():
        selected[obj] = process_nested_fields(changes, fields)
    return selected.items()


def prepare_dict(reporter: MailReporter, objects: dict, k_name, *args, **fields):
    """
    @param reporter: reporter
    @param objects: changed objects
    @param k_name: field that will be used as the key of the dictionary entries
    @param args: not used
    @param fields: fields of foreign key objects to be included in the dictionary
    @return: a Python dictionary that is serializable
    """
    serializable = {}
    for obj, changes in objects.items():
        serializable[str(getattr(obj, k_name))] = process_nested_fields(changes, fields)
    reporter.last_changes = serializable
    reporter.save()
    return serializable


def get_json(reporter, objects: dict, k_name="name", *args, **fields):
    """
    @param reporter: reporter
    @param objects: changed objects
    @param k_name: field that will be used as the key of the JSON file entries
    @param args: not used
    @param fields: fields of foreign key objects to be included in the JSON
    @return: a JSON string
    """
    return json.dumps(prepare_dict(reporter, objects, k_name, *args, **fields), ensure_ascii=True, indent=2)


def report_worker(reporter):
    """
    Worker used in threads, handles exceptions and saves them to the 'Messages' field
    @param reporter: an API connector
    @return: None
    """
    try:
        report_single(reporter)
    except Exception as e:
        set_status(reporter, FAIL, f"Failed: {e}")
    return


def start_thread(reporter):
    """
    Starts a new thread
    @param reporter: reporter
    @return: None
    """
    t = Thread(target=report_worker, args=(reporter,))
    t.daemon = False
    t.start()
