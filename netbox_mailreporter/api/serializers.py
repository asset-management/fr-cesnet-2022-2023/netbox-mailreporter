from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer
from netbox_mailreporter.models import *


class MailReporterSerializer(NetBoxModelSerializer):
    class Meta:
        model = MailReporter
        fields = '__all__'

