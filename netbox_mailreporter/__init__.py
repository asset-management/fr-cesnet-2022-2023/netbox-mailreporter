from django.db.models.signals import post_save

from extras.plugins import PluginConfig


class NetboxMailReporter(PluginConfig):
    name = "netbox_mailreporter"
    verbose_name = "mailreporter"
    version = "0.1.7"
    base_url = "netbox_mailreporter"
    min_version = "3.4"
    required_settings = []
    default_settings = {
        "FROM_EMAIL": "reporter@muni.cz"
    }

    # def ready(self):
    #     try:
    #         super().ready()
    #         from netbox_mailreporter.models import MailReporter
    #         for o in MailReporter.objects.all():
    #             o.save()
    #     except Exception as e:
    #         print(f"An exception occurred during initializing MailReporters: {e}")


config = NetboxMailReporter
