# Generated by Django 4.1.8 on 2023-06-05 08:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('netbox_mailreporter', '0014_alter_mailreporter_email_attachment_filename'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mailreporter',
            name='email_attachment_filename',
            field=models.CharField(blank=True, default='changes.txt', max_length=255, null=True),
        ),
    ]
